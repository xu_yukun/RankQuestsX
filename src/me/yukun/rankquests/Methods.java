package me.yukun.rankquests;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class Methods {
	public static Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("RankQuests");
	private static Methods instance = new Methods();
	private static Config config = Config.getInstance();

	public static Methods getInstance() {
		return instance;
	}

	@SuppressWarnings("deprecation")
	public ItemStack getItemInHand(Player player) {
		if (getVersion() >= 191) {
			return player.getInventory().getItemInMainHand();
		} else {
			return player.getItemInHand();
		}
	}

	public boolean containsItem(Player player, ItemStack item) {
		for (ItemStack items : player.getInventory().getContents()) {
			if (items != null) {
				if (items.isSimilar(item) && (items.getAmount() + item.getAmount()) <= 64) {
					return true;
				} else {
					continue;
				}
			} else {
				continue;
			}
		}
		return false;
	}

	public ItemStack getItem(Player player, ItemStack item) {
		if (containsItem(player, item)) {
			for (ItemStack items : player.getInventory().getContents()) {
				if (items != null) {
					if (items.isSimilar(item) && (items.getAmount() + item.getAmount()) <= 64) {
						return items;
					} else {
						continue;
					}
				} else {
					continue;
				}
			}
		}
		return null;
	}

	public int getItemSlot(Player player, ItemStack item) {
		if (containsItem(player, item)) {
			for (int i = 0; i <= 35; ++i) {
				ItemStack items = player.getInventory().getItem(i);
				if (items != null) {
					if (items.isSimilar(item) && (items.getAmount() + item.getAmount()) <= 64) {
						return i;
					} else {
						continue;
					}
				} else {
					continue;
				}
			}
		}
		return -1;
	}

	@SuppressWarnings("deprecation")
	public void setItemInHand(Player player, ItemStack item) {
		if (getVersion() >= 191) {
			player.getInventory().setItemInMainHand(item);
		} else {
			player.setItemInHand(item);
		}
	}

	public String color(String msg) {
		return ChatColor.translateAlternateColorCodes('&', msg);
	}

	public String removeColor(String msg) {
		msg = ChatColor.stripColor(msg);
		return msg;
	}

	public String replacePHolders(String msg, Player player, Rank rank) {
		String rankname = rank.getRankname();
		if (!config.isSplit()) {
			return msg.replace("%rank%", rankname).replace("%player%", player.getDisplayName());
		} else {
			return msg.replace("%rank%", rankname).replace("%player%", player.getDisplayName());
		}
	}

	public Integer getVersion() {
		String ver = Bukkit.getServer().getClass().getPackage().getName();
		ver = ver.substring(ver.lastIndexOf('.') + 1);
		ver = ver.replaceAll("_", "").replaceAll("R", "").replaceAll("v", "");
		return Integer.parseInt(ver);
	}

	public Integer getArgument(String Argument, ItemStack item, String Msg) {
		String arg = "0";
		Msg = color(Msg).toLowerCase();
		String name = item.getItemMeta().getDisplayName().toLowerCase();
		if (Msg.contains(Argument.toLowerCase())) {
			String[] b = Msg.split(Argument.toLowerCase());
			if (b.length >= 1)
				arg = name.replace(b[0], "");
			if (b.length >= 2)
				arg = arg.replace(b[1], "");
		}
		if (isInt(arg)) {
			return Integer.parseInt(arg);
		} else {
			return null;
		}
	}

	public String getString(String Argument, ItemStack item, String Msg) {
		String arg = "0";
		Msg = color(Msg).toLowerCase();
		String name = item.getItemMeta().getDisplayName().toLowerCase();
		if (Msg.contains(Argument.toLowerCase())) {
			String[] b = Msg.split(Argument.toLowerCase());
			if (b.length >= 1)
				arg = name.replace(b[0], "");
			if (b.length >= 2)
				arg = arg.replace(b[1], "");
		}
		return arg;
	}

	public Boolean isInt(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public Player getPlayer(String name) {
		return Bukkit.getServer().getPlayer(name);
	}

	public Boolean hasArgument(String Argument, List<String> Msg) {
		for (String l : Msg) {
			l = color(l).toLowerCase();
			if (l.contains(Argument.toLowerCase())) {
				return true;
			}
		}
		return false;
	}
}