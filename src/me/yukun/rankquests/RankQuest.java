package me.yukun.rankquests;

import org.bukkit.entity.Player;

public class RankQuest {
	private Rank rank;
	private Player player;
	private Integer timer;
	private Integer time;
	private Integer slot;

	public RankQuest(Rank rank, Player player, Integer timer, Integer time, Integer slot) {
		this.rank = rank;
		this.player = player;
		this.timer = timer;
		this.time = time;
		this.slot = slot;
	}

	public RankQuest setRank(Rank rank) {
		this.rank = rank;
		return this;
	}

	public RankQuest setPlayer(Player player) {
		this.player = player;
		return this;
	}

	public RankQuest setTimer(Integer timer) {
		this.timer = timer;
		return this;
	}

	public RankQuest setTime(Integer time) {
		this.time = time;
		return this;
	}

	public RankQuest setSlot(Integer slot) {
		this.slot = slot;
		return this;
	}

	public Rank getRank() {
		return rank;
	}

	public Player getPlayer() {
		return player;
	}

	public Integer getTimer() {
		return timer;
	}

	public Integer getTime() {
		return time;
	}

	public Integer getSlot() {
		return slot;
	}
}