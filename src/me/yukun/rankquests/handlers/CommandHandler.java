package me.yukun.rankquests.handlers;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.yukun.rankquests.Config;
import me.yukun.rankquests.Main;
import me.yukun.rankquests.Methods;
import me.yukun.rankquests.Rank;

public class CommandHandler implements CommandExecutor {
	String SIname = "noitem";
	String header = "&b&l==========RankQuests v1.0 by Yukun==========";
	String footer = "&b&l============================================";
	String usage = "/rankquest list";
	String usage2 = "/rankquest give <player> <rank> <amount>";
	String usage3 = "/rankquest reload";
	String usage4 = "/rankquest redeem";
	String listheader = "&b&lList of quests:";
	private Config config = Config.getInstance();
	private Methods methods = Methods.getInstance();
	private ItemHandler itemhandler = ItemHandler.getInstance();
	private QuestHandler questhandler = QuestHandler.getInstance();
	private MessageHandler messagehandler = MessageHandler.getInstance();

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (args.length == 0) {
			sender.sendMessage(methods.color(header));
			sender.sendMessage(methods.color("&fThanks for downloading my plugin!"));
			sender.sendMessage(methods.color("&bCommands: "));
			sender.sendMessage(methods.color("&c/rankquest list: " + usage));
			sender.sendMessage(methods.color("&c/rankquest give: " + usage2));
			sender.sendMessage(methods.color("&c/rankquest reload: " + usage3));
			sender.sendMessage(methods.color("&c/rankquest redeem: " + usage4));
			sender.sendMessage(methods.color(footer));
			return false;
		} else if (args.length == 1) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (args[0].equalsIgnoreCase("redeem")) {
					if (config.getRedeemString("Redeem." + player.getName()) != null) {
						HashMap<String, Integer> redeems = new HashMap<String, Integer>();
						if (Main.settings.getRedeems().getConfigurationSection("Redeem." + player.getName()).getKeys(false) != null) {
							for (String line : Main.settings.getRedeems().getConfigurationSection("Redeem." + player.getName()).getKeys(false)) {
								redeems.put(line, Integer.parseInt(config.getRedeemString("Redeem." + player.getName() + "." + line + ".Amount")));
							}
							if (redeems.size() > 0) {
								for (Rank rank : questhandler.getAllRanks()) {
									String ranks = rank.getName();
									if (redeems.get(ranks) != null) {
										ItemStack item = itemhandler.makeQuest(player, rank, redeems.get(ranks));
										if (player.getInventory().firstEmpty() != -1) {
											player.getInventory().addItem(item);
											redeems.remove(ranks);
											config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", null);
											Main.settings.saveRedeems();
											config.setRedeemsString("Redeem." + player.getName() + "." + ranks, null);
											Main.settings.saveRedeems();
											config.setRedeemsString("Redeem." + player.getName(), null);
											Main.settings.saveRedeems();
											return true;
										} else if (methods.containsItem(player, item)) {
											ItemStack invenitem = methods.getItem(player, item);
											if ((invenitem.getAmount() + item.getAmount()) <= 64) {
												player.getInventory().addItem(item);
												messagehandler.questReceive(player, rank, item.getAmount());
												redeems.remove(ranks);
												config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", null);
												Main.settings.saveRedeems();
												config.setRedeemsString("Redeem." + player.getName() + "." + ranks, null);
												Main.settings.saveRedeems();
												config.setRedeemsString("Redeem." + player.getName(), null);
												Main.settings.saveRedeems();
												return true;
											} else {
												messagehandler.questInvenFull(player);
												return false;
											}
										} else {
											messagehandler.questInvenFull(player);
											return false;
										}
									}
									continue;
								}
								messagehandler.invalid(sender);
								return false;
							} else {
								messagehandler.invalid(sender);
								return false;
							}
						} else {
							messagehandler.invalid(sender);
							return false;
						}
					} else {
						messagehandler.invalid(sender);
						return false;
					}
				} else if (player.isOp() || player.hasPermission("RankQuest.Admin")) {
					if (args[0].equalsIgnoreCase("reload")) {
						questhandler.cancelAllQuests();
						Main.settings.setup(Bukkit.getPluginManager().getPlugin("RankQuests"));
						Main.settings.reloadConfig();
						Main.settings.reloadMessages();
						Main.settings.reloadQuests();
						questhandler.reload();
						messagehandler.reload();
						messagehandler.reloaded(sender);
						return true;
					} else if (args[0].equalsIgnoreCase("give")) {
						sender.sendMessage(methods.color(usage2));
						return false;
					} else if (args[0].equalsIgnoreCase("list")) {
						player.sendMessage(methods.color(listheader));
						for (Rank rank : questhandler.getAllRanks()) {
							String rankname = rank.getRankname();
							String name = rank.getName();
							player.sendMessage(methods.color(name + ": " + rankname));
						}
						return true;
					} else {
						messagehandler.invalid(sender);
						return false;
					}
				} else if (player.hasPermission("RankQuest.Reload")) {
					if (args[0].equalsIgnoreCase("reload")) {
						questhandler.cancelAllQuests();
						Main.settings.setup(Bukkit.getPluginManager().getPlugin("RankQuests"));
						Main.settings.reloadConfig();
						Main.settings.reloadMessages();
						Main.settings.reloadQuests();
						questhandler.reload();
						messagehandler.reload();
						messagehandler.reloaded(sender);
						return true;
					} else if (args[0].equalsIgnoreCase("give")) {
						sender.sendMessage(methods.color(usage2));
						return false;
					} else {
						messagehandler.invalid(sender);
						return false;
					}
				} else {
					messagehandler.noPerms(sender);
					return false;
				}
			} else {
				if (args[0].equalsIgnoreCase("reload")) {
					questhandler.cancelAllQuests();
					Main.settings.setup(Bukkit.getPluginManager().getPlugin("RankQuests"));
					Main.settings.reloadConfig();
					Main.settings.reloadMessages();
					Main.settings.reloadQuests();
					questhandler.reload();
					messagehandler.reload();
					messagehandler.reloaded(sender);
					return true;
				} else if (args[0] == "give") {
					sender.sendMessage(methods.color(usage2));
					return false;
				} else {
					messagehandler.invalid(sender);
					return false;
				}
			}
		} else if (args.length == 2) {
			if (args[0].equalsIgnoreCase("give")) {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					if (player.isOp() || player.hasPermission("RankQuest.Give") || player.hasPermission("RankQuest.Admin")) {
						for (Rank rank : questhandler.getAllRanks()) {
							String ranks = rank.getName();
							if (args[1].equalsIgnoreCase(ranks)) {
								ItemStack item = itemhandler.makeQuest(player, rank, 1);
								if (player.getInventory().firstEmpty() != -1) {
									messagehandler.questReceive(player, rank, 1);
									player.getInventory().addItem(item);
									return true;
								} else {
									if (!methods.containsItem(player, item)) {
										messagehandler.questInvenFull(player);
										config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", 1);
										return true;
									} else {
										ItemStack invenitem = methods.getItem(player, item);
										if ((invenitem.getAmount() + 1) <= 64) {
											messagehandler.questReceive(player, rank, 1);
											player.getInventory().addItem(item);
											return true;
										} else {
											config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", 1);
											messagehandler.questInvenFull(player);
											return false;
										}
									}
								}
							} else if (args[1].equalsIgnoreCase(ranks + "Voucher")) {
								ItemStack item = itemhandler.makeVoucher(player, rank, 1);
								if (player.getInventory().firstEmpty() != -1) {
									messagehandler.voucherReceive(player, rank, 1);
									player.getInventory().addItem(item);
									return true;
								} else {
									if (!methods.containsItem(player, item)) {
										messagehandler.voucherInvenFull(player);
										return true;
									} else {
										ItemStack invenitem = methods.getItem(player, item);
										if ((invenitem.getAmount() + 1) <= 64) {
											messagehandler.voucherReceive(player, rank, 1);
											player.getInventory().addItem(item);
											return true;
										} else {
											messagehandler.voucherInvenFull(player);
											return false;
										}
									}
								}
							}
							continue;
						}
						messagehandler.invalid(sender);
						return false;
					} else {
						messagehandler.noPerms(sender);
						return false;
					}
				} else {
					messagehandler.specify(sender);
					return false;
				}
			} else {
				messagehandler.invalid(sender);
				return false;
			}
		} else if (args.length == 3) {
			if (args[0].equalsIgnoreCase("give")) {
				if (sender instanceof Player) {
					if (sender.isOp() || sender.hasPermission("RankQuest.Give") || sender.hasPermission("RankQuest.Admin")) {
						if (Bukkit.getPlayer(args[1]) != null) {
							Player player = Bukkit.getPlayer(args[1]);
							for (Rank rank : questhandler.getAllRanks()) {
								String ranks = rank.getName();
								if (args[2].equalsIgnoreCase(ranks)) {
									ItemStack item = itemhandler.makeQuest(player, rank, 1);
									if (player.getInventory().firstEmpty() != -1) {
										messagehandler.questReceive(player, rank, 1);
										player.getInventory().addItem(item);
										return true;
									} else {
										if (!methods.containsItem(player, item)) {
											messagehandler.questInvenFull(player);
											config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", 1);
											return false;
										} else {
											ItemStack invenitem = methods.getItem(player, item);
											if ((invenitem.getAmount() + 1) <= 64) {
												messagehandler.questReceive(player, rank, 1);
												player.getInventory().addItem(item);
												return true;
											} else {
												config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", 1);
												messagehandler.questInvenFull(player);
												return false;
											}
										}
									}
								} else if (args[2].equalsIgnoreCase(ranks + "Voucher")) {
									ItemStack item = itemhandler.makeVoucher(player, rank, 1);
									if (player.getInventory().firstEmpty() != -1) {
										messagehandler.voucherReceive(player, rank, 1);
										player.getInventory().addItem(item);
										return true;
									} else {
										if (!methods.containsItem(player, item)) {
											messagehandler.voucherInvenFull(player);
											return false;
										} else {
											ItemStack invenitem = methods.getItem(player, item);
											if ((invenitem.getAmount() + 1) <= 64) {
												messagehandler.voucherReceive(player, rank, 1);
												player.getInventory().addItem(item);
												return true;
											} else {
												messagehandler.voucherInvenFull(player);
												return false;
											}
										}
									}
								}
								continue;
							}
							messagehandler.invalid(sender);
							return false;
						} else {
							messagehandler.invalid(sender);
							return false;
						}
					} else {
						messagehandler.noPerms(sender);
						return false;
					}
				} else {
					if (Bukkit.getPlayer(args[1]) != null) {
						Player player = Bukkit.getPlayer(args[1]);
						for (Rank rank : questhandler.getAllRanks()) {
							String ranks = rank.getName();
							if (args[2].equalsIgnoreCase(ranks)) {
								ItemStack item = itemhandler.makeQuest(player, rank, 1);
								if (player.getInventory().firstEmpty() != -1) {
									messagehandler.questReceive(player, rank, 1);
									player.getInventory().addItem(item);
									return true;
								} else {
									if (!methods.containsItem(player, item)) {
										config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", 1);
										messagehandler.questInvenFull(player);
										return false;
									} else {
										ItemStack invenitem = methods.getItem(player, item);
										if ((invenitem.getAmount() + 1) <= 64) {
											messagehandler.questReceive(player, rank, 1);
											player.getInventory().addItem(item);
											return true;
										} else {
											config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", 1);
											messagehandler.questInvenFull(player);
											return false;
										}
									}
								}
							} else if (args[2].equalsIgnoreCase(ranks + "Voucher")) {
								ItemStack item = itemhandler.makeVoucher(player, rank, 1);
								if (player.getInventory().firstEmpty() != -1) {
									messagehandler.voucherReceive(player, rank, 1);
									player.getInventory().addItem(item);
									return true;
								} else {
									if (!methods.containsItem(player, item)) {
										messagehandler.voucherInvenFull(player);
										return true;
									} else {
										ItemStack invenitem = methods.getItem(player, item);
										if ((invenitem.getAmount() + 1) <= 64) {
											messagehandler.voucherReceive(player, rank, 1);
											player.getInventory().addItem(item);
											return true;
										} else {
											messagehandler.voucherInvenFull(player);
											return false;
										}
									}
								}
							}
							continue;
						}
						messagehandler.invalid(sender);
						return false;
					} else {
						messagehandler.invalid(sender);
						return false;
					}
				}
			} else {
				messagehandler.invalid(sender);
				return false;
			}
		} else if (args.length == 4) {
			if (args[0].equalsIgnoreCase("give")) {
				if (sender instanceof Player) {
					if (sender.isOp() || sender.hasPermission("RankQuest.Give") || sender.hasPermission("RankQuest.Admin")) {
						if (Bukkit.getPlayer(args[1]) != null) {
							Player player = Bukkit.getPlayer(args[1]);
							for (Rank rank : questhandler.getAllRanks()) {
								String ranks = rank.getName();
								if (args[2].equalsIgnoreCase(ranks)) {
									if (methods.isInt(args[3])) {
										int amount = Integer.parseInt(args[3]);
										if (amount != 0 && amount <= 64) {
											ItemStack item = itemhandler.makeQuest(player, rank, amount);
											if (player.getInventory().firstEmpty() != -1) {
												player.getInventory().addItem(item);
												messagehandler.questReceive(player, rank, amount);
												return true;
											} else {
												if (!methods.containsItem(player, item)) {
													messagehandler.questInvenFull(player);
													config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", amount);
													return false;
												} else {
													ItemStack invenitem = methods.getItem(player, item);
													if ((invenitem.getAmount() + amount) <= 64) {
														messagehandler.questReceive(player, rank, amount);
														player.getInventory().addItem(item);
														return true;
													} else {
														messagehandler.questInvenFull(player);
														config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", amount);
														return false;
													}
												}
											}
										} else {
											messagehandler.overStack(sender);
											return false;
										}
									} else {
										messagehandler.invalid(sender);
										return false;
									}
								} else if (args[2].equalsIgnoreCase(ranks + "Voucher")) {
									if (methods.isInt(args[3])) {
										int amount = Integer.parseInt(args[3]);
										if (amount != 0 && amount <= 64) {
											ItemStack item = itemhandler.makeVoucher(player, rank, amount);
											if (player.getInventory().firstEmpty() != -1) {
												player.getInventory().addItem(item);
												messagehandler.voucherReceive(player, rank, amount);
												return true;
											} else {
												if (!methods.containsItem(player, item)) {
													messagehandler.voucherInvenFull(player);
													return false;
												} else {
													ItemStack invenitem = methods.getItem(player, item);
													if ((invenitem.getAmount() + amount) <= 64) {
														messagehandler.voucherReceive(player, rank, amount);
														player.getInventory().addItem(item);
														return true;
													} else {
														messagehandler.voucherInvenFull(player);
														return false;
													}
												}
											}
										}
									}
								}
								continue;
							}
							messagehandler.invalid(sender);
							return false;
						} else {
							messagehandler.invalid(sender);
							return false;
						}
					} else {
						messagehandler.noPerms(sender);
						return false;
					}
				} else {
					if (Bukkit.getPlayer(args[1]) != null) {
						Player player = Bukkit.getPlayer(args[1]);
						for (Rank rank : questhandler.getAllRanks()) {
							String ranks = rank.getName();
							if (args[2].equalsIgnoreCase(ranks)) {
								if (methods.isInt(args[3])) {
									int amount = Integer.parseInt(args[3]);
									if (amount != 0 && amount <= 64) {
										ItemStack item = itemhandler.makeQuest(player, rank, amount);
										if (player.getInventory().firstEmpty() != -1) {
											player.getInventory().addItem(item);
											messagehandler.questReceive(player, rank, amount);
											return true;
										} else {
											if (!methods.containsItem(player, item)) {
												config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", amount);
												messagehandler.questInvenFull(player);
												return false;
											} else {
												ItemStack invenitem = methods.getItem(player, item);
												if ((invenitem.getAmount() + amount) <= 64) {
													messagehandler.questReceive(player, rank, amount);
													player.getInventory().addItem(item);
													return true;
												} else {
													config.setRedeemsInt("Redeem." + player.getName() + "." + ranks + ".Amount", amount);
													messagehandler.questInvenFull(player);
													return false;
												}
											}
										}
									} else {
										messagehandler.overStack(sender);
										return false;
									}
								} else {
									messagehandler.invalid(sender);
									return false;
								}
							} else if (args[2].equalsIgnoreCase(ranks + "Voucher")) {
								if (methods.isInt(args[3])) {
									int amount = Integer.parseInt(args[3]);
									if (amount != 0 && amount <= 64) {
										ItemStack item = itemhandler.makeVoucher(player, rank, amount);
										if (player.getInventory().firstEmpty() != -1) {
											player.getInventory().addItem(item);
											messagehandler.voucherReceive(player, rank, amount);
											return true;
										} else {
											if (!methods.containsItem(player, item)) {
												messagehandler.voucherInvenFull(player);
												return false;
											} else {
												ItemStack invenitem = methods.getItem(player, item);
												if ((invenitem.getAmount() + amount) <= 64) {
													messagehandler.voucherReceive(player, rank, amount);
													player.getInventory().addItem(item);
													return true;
												} else {
													messagehandler.voucherInvenFull(player);
													return false;
												}
											}
										}
									} else {
										messagehandler.overStack(sender);
										return false;
									}
								} else {
									messagehandler.invalid(sender);
									return false;
								}
							}
							continue;
						}
						messagehandler.invalid(sender);
						return false;
					} else {
						messagehandler.invalid(sender);
						return false;
					}
				}
			} else {
				messagehandler.invalid(sender);
				return false;
			}
		} else if (args.length > 4) {
			messagehandler.invalid(sender);
			return false;
		} else {
			messagehandler.invalid(sender);
			return false;
		}
	}
}