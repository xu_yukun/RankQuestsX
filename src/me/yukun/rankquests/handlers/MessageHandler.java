package me.yukun.rankquests.handlers;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.yukun.rankquests.Config;
import me.yukun.rankquests.Methods;
import me.yukun.rankquests.Rank;

public class MessageHandler {
	private Config config = Config.getInstance();
	private Methods methods = Methods.getInstance();
	private QuestHandler questhandler;
	private static MessageHandler instance = new MessageHandler();
	private ArrayList<String> queststart = new ArrayList<String>();
	private ArrayList<String> questallstart = new ArrayList<String>();
	private String questcomplete = "";
	private String questallcomplete = "";
	private String voucheruse = "";
	private String notInWarzone = config.getMessageString("Messages.NotInWarzone");
	private String only1RankQuest = config.getMessageString("Messages.Only1RankQuest");
	private String prefix = config.getMessageString("Messages.Prefix");
	private String move = config.getMessageString("Messages.NoMovingQuest");
	private String drop = config.getMessageString("Messages.DropItemMessage");
	private String onlystart1 = config.getMessageString("Messages.OnlyStart1");
	private String reloaded = "&aConfig files reloaded!";
	private String invalid = "&cInvalid argument!";
	private String noperms = "&cYou do not have permission to use this command!";
	private String specify = "&cPlease specify who to send the item to!";
	private String overstack = "&cEither you are trying to spawn in 0 items, or more than a stack of items. I'm sorry, but you can't do that.";
	private String questreceive = config.getMessageString("Messages.QuestReceive");
	private String voucherreceive = config.getMessageString("Messages.VoucherReceive");
	private String fullinvenquest = config.getMessageString("Messages.QuestInventoryFull");
	private String fullinvenvoucher = config.getMessageString("Messages.VoucherInventoryFull");

	private HashMap<Rank, ArrayList<String>> squeststart = new HashMap<Rank, ArrayList<String>>();
	private HashMap<Rank, ArrayList<String>> squestallstart = new HashMap<Rank, ArrayList<String>>();
	private HashMap<Rank, String> squestcomplete = new HashMap<Rank, String>();
	private HashMap<Rank, String> squestallcomplete = new HashMap<Rank, String>();
	private HashMap<Rank, String> svoucheruse = new HashMap<Rank, String>();

	public static MessageHandler getInstance() {
		return instance;
	}

	public void onEnable() {
		questhandler = QuestHandler.getInstance();
		notInWarzone = config.getMessageString("Messages.NotInWarzone");
		only1RankQuest = config.getMessageString("Messages.Only1RankQuest");
		prefix = config.getMessageString("Messages.Prefix");
		move = config.getMessageString("Messages.NoMovingQuest");
		drop = config.getMessageString("Messages.DropItemMessage");
		onlystart1 = config.getMessageString("Messages.OnlyStart1");
		if (!config.isSplit()) {
			queststart = (ArrayList<String>) config.getMessageStringList("Messages.BeginMessage");
			questallstart = (ArrayList<String>) config.getMessageStringList("Messages.BeginAllMessage");
			questcomplete = config.getMessageString("Messages.QuestComplete");
			questallcomplete = config.getMessageString("Messages.QuestAllComplete");
			voucheruse = config.getMessageString("Messages.VoucherUse");
			return;
		} else {
			for (Rank rank : questhandler.getAllRanks()) {
				String crank = rank.getName();
				squeststart.put(rank, (ArrayList<String>) config.getQuestsStringList("Quests." + crank + ".Messages.BeginMessage"));
				squestallstart.put(rank, (ArrayList<String>) config.getQuestsStringList("Quests." + crank + ".Messages.BeginAllMessage"));
				squestcomplete.put(rank, config.getQuestsString("Quests." + crank + ".Messages.QuestComplete"));
				squestallcomplete.put(rank, config.getQuestsString("Quests." + crank + ".Messages.QuestAllComplete"));
				svoucheruse.put(rank, config.getQuestsString("Quests." + crank + ".Messages.VoucherUse"));
				continue;
			}
			return;
		}
	}

	public void reload() {
		questhandler = QuestHandler.getInstance();
		if (config.isSplit()) {
			squeststart.clear();
			squestallstart.clear();
			squestcomplete.clear();
			squestallcomplete.clear();
			svoucheruse.clear();
			for (Rank rank : questhandler.getAllRanks()) {
				String crank = rank.getName();
				squeststart.put(rank, (ArrayList<String>) config.getQuestsStringList("Quests." + crank + ".Messages.BeginMessage"));
				squestallstart.put(rank, (ArrayList<String>) config.getQuestsStringList("Quests." + crank + ".Messages.BeginAllMessage"));
				squestcomplete.put(rank, config.getQuestsString("Quests." + crank + ".Messages.QuestComplete"));
				squestallcomplete.put(rank, config.getQuestsString("Quests." + crank + ".Messages.QuestAllComplete"));
				svoucheruse.put(rank, config.getQuestsString("Quests." + crank + ".Messages.VoucherUse"));
				continue;
			}
			return;
		}
	}

	public void startQuest(Rank rank, Player player) {
		if (!config.isSplit()) {
			for (String begin : queststart) {
				player.sendMessage(methods.color(methods.replacePHolders(begin, player, rank)));
			}
			for (Player online : Bukkit.getServer().getOnlinePlayers()) {
				if (online != player) {
					for (String beginall : questallstart) {
						online.sendMessage(methods.color(methods.replacePHolders(beginall, player, rank)));
					}
				}
			}
		} else {
			if (rank != null) {
				for (String begin : squeststart.get(rank)) {
					player.sendMessage(methods.color(methods.replacePHolders(begin, player, rank)));
				}
				for (Player online : Bukkit.getServer().getOnlinePlayers()) {
					if (online != player) {
						for (String beginall : squestallstart.get(rank)) {
							online.sendMessage(methods.color(methods.replacePHolders(beginall, player, rank)));
						}
					}
				}
			} else {
				return;
			}
		}
		return;
	}

	public void endQuest(Player player, Rank rank) {
		if (!config.isSplit()) {
			player.sendMessage(methods.color(methods.replacePHolders(prefix + questcomplete, player, rank)));
			for (Player players : Bukkit.getServer().getOnlinePlayers()) {
				if (players != player) {
					players.sendMessage(methods.color(methods.replacePHolders(prefix + questallcomplete, player, rank)));
				}
			}
		} else {
			player.sendMessage(methods.color(methods.replacePHolders(prefix + squestcomplete.get(rank), player, rank)));
			for (Player players : Bukkit.getServer().getOnlinePlayers()) {
				if (players != player) {
					players.sendMessage(methods.color(methods.replacePHolders(prefix + squestallcomplete.get(rank), player, rank)));
				}
			}
		}
		return;
	}

	public void noMoveQuestItem(Player player) {
		player.sendMessage(methods.color(prefix + move));
		return;
	}

	public void noDropQuestItem(Player player) {
		player.sendMessage(methods.color(prefix + drop));
		return;
	}

	public void only1QuestItem(Player player) {
		player.sendMessage(methods.color(prefix + only1RankQuest));
		return;
	}

	public void only1Quest(Player player) {
		player.sendMessage(methods.color(prefix + onlystart1));
		return;
	}

	public void notWarzone(Player player) {
		player.sendMessage(methods.color(prefix + notInWarzone));
		return;
	}

	public void voucherUse(Player player, Rank rank) {
		if (!config.isSplit()) {
			player.sendMessage(methods.color(methods.replacePHolders(prefix + voucheruse, player, rank)));
		} else {
			player.sendMessage(methods.color(methods.replacePHolders(prefix + svoucheruse.get(rank), player, rank)));
		}
		return;
	}

	public void invalid(CommandSender sender) {
		sender.sendMessage(methods.color(prefix + invalid));
		return;
	}

	public void noPerms(CommandSender sender) {
		sender.sendMessage(methods.color(prefix + noperms));
		return;
	}

	public void specify(CommandSender sender) {
		sender.sendMessage(methods.color(prefix + specify));
		return;
	}

	public void overStack(CommandSender sender) {
		sender.sendMessage(methods.color(prefix + overstack));
		return;
	}

	public void reloaded(CommandSender sender) {
		sender.sendMessage(methods.color(prefix + reloaded));
		return;
	}

	public void questReceive(Player player, Rank rank, Integer amount) {
		player.sendMessage(methods.color(methods.replacePHolders(prefix + questreceive, player, rank).replace("%amount%", amount + "")));
		return;
	}

	public void voucherReceive(Player player, Rank rank, Integer amount) {
		player.sendMessage(methods.color(methods.replacePHolders(prefix + voucherreceive, player, rank).replace("%amount%", amount + "")));
		return;
	}

	public void questInvenFull(Player player) {
		player.sendMessage(methods.color(prefix + fullinvenquest));
		return;
	}

	public void voucherInvenFull(Player player) {
		player.sendMessage(methods.color(prefix + fullinvenvoucher));
		return;
	}
}