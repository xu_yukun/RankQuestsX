package me.yukun.rankquests.handlers;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import me.yukun.rankquests.Config;
import me.yukun.rankquests.Rank;
import me.yukun.rankquests.RankQuest;
import me.yukun.rankquests.multisupport.Support;

public class QuestHandler {
	public Plugin plugin = Bukkit.getPluginManager().getPlugin("RankQuests");
	private Config config = Config.getInstance();
	private ItemHandler itemhandler = ItemHandler.getInstance();
	private MessageHandler messagehandler = MessageHandler.getInstance();
	private Support support = Support.getInstance();
	private ArrayList<Rank> ranks = new ArrayList<Rank>();
	private ArrayList<RankQuest> rankquests = new ArrayList<RankQuest>();
	private ArrayList<Player> flying = new ArrayList<Player>();
	private static QuestHandler instance = new QuestHandler();

	public static QuestHandler getInstance() {
		return instance;
	}

	public void onEnable() {
		if (!config.isSplit()) {
			if (config.getConfigConfigSection("RankQuestOptions.Ranks") != null) {
				for (String crank : config.getConfigConfigSection("RankQuestOptions.Ranks")) {
					String rankname = config.getConfigString("RankQuestOptions.Ranks." + crank + ".RankName");
					int maxtime = config.getConfigInt("RankQuestOptions.Ranks." + crank + ".Time");
					Boolean checkWarzone = config.getQuestsBoolean("Quests." + crank + ".CheckWarzone");
					Boolean checkWorldGuard = config.getQuestsBoolean("Quests." + crank + ".CheckWorldGuard");
					Boolean checkBlacklist = config.getQuestsBoolean("Quests." + crank + ".CheckBlacklist");
					Boolean checkPvP = config.getQuestsBoolean("Quests." + crank + ".PvPFlag");
					ArrayList<String> regions = (ArrayList<String>) config.getQuestsStringList("Quests." + crank + ".Regions");
					ArrayList<String> blacklist = (ArrayList<String>) config.getQuestsStringList("Quests." + crank + ".RegionBlacklist");
					Rank rank = new Rank(crank, rankname, maxtime, checkWarzone, checkWorldGuard, checkBlacklist, checkPvP, regions, blacklist);
					ranks.add(rank);
				}
			}
		} else {
			if (config.getConfigConfigSection("RankQuestOptions.Ranks") != null) {
				for (String crank : config.getConfigConfigSection("RankQuestOptions.Ranks")) {
					String rankname = config.getConfigString("RankQuestOptions.Ranks." + crank + ".RankName");
					int maxtime = config.getConfigInt("RankQuestOptions.Ranks." + crank + ".Time");
					Boolean checkWarzone = config.getConfigBoolean("RankQuestOptions.CheckWarzone");
					Boolean checkWorldGuard = config.getConfigBoolean("RankQuestOptions.CheckWorldGuard");
					Boolean checkBlacklist = config.getConfigBoolean("RankQuestOptions.CheckBlacklist");
					Boolean checkPvP = config.getConfigBoolean("RankQuestOptions.PvPFlag");
					ArrayList<String> regions = (ArrayList<String>) config.getConfigStringList("RankQuestOptions.Regions");
					ArrayList<String> blacklist = (ArrayList<String>) config.getConfigStringList("RankQuestOptions.RegionBlacklist");
					Rank rank = new Rank(crank, rankname, maxtime, checkWarzone, checkWorldGuard, checkBlacklist, checkPvP, regions, blacklist);
					ranks.add(rank);
				}
			}
		}
	}

	public void reload() {
		ranks.clear();
		onEnable();
	}

	public Boolean isQuestSlot(RankQuest rq, Integer slot) {
		return rq.getSlot() == slot;
	}

	/**
	 * Gets rank quest that is being done by specified player.
	 * 
	 * @param player
	 *            Player to get rank quest for.
	 * @return Rank quest being done by specified player.
	 */
	public RankQuest getQuestFromPlayer(Player player) {
		if (rankquests.size() > 0) {
			for (RankQuest rankquest : rankquests) {
				if (rankquest.getPlayer() == player) {
					return rankquest;
				}
			}
		}
		return null;
	}

	public Rank getRankFromName(String name) {
		for (Rank rank : getAllRanks()) {
			if (name.equals(rank.getName())) {
				return rank;
			}
		}
		return null;
	}

	/**
	 * Gets a list of all ranks.
	 * 
	 * @return List of all ranks.
	 */
	public ArrayList<Rank> getAllRanks() {
		return ranks;
	}

	/**
	 * Starts quest of specicified rank for specified player.
	 * 
	 * @param player
	 *            Player to start quest of specified rank for.
	 * @param rank
	 *            Rank of quest to be started for specified player.
	 * @param slot
	 *            Held item slot of specified player.
	 */
	public void startQuest(Player player, Rank rank, int slot) {
		// Makes countdown item.
		ItemStack item = itemhandler.makeCdQuest(player, rank, 1, rank.getMaxtime());
		if (player.getGameMode() != GameMode.CREATIVE) {
			if (player.isFlying()) {
				player.setFlying(false);
			}
		}
		if (player.getGameMode() != GameMode.CREATIVE) {
			if (player.getAllowFlight()) {
				player.setAllowFlight(false);
				flying.add(player);
			}
		}
		// Gives player max time item.
		player.getInventory().setItem(slot, item);
		// Starts player timer.
		int time = rank.getMaxtime();
		RankQuest rankquest = new RankQuest(rank, player, 0, time, slot);
		rankquests.add(rankquest);
		int timer = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			@Override
			public void run() {
				if (canQuest(player, getQuestFromPlayer(player).getRank())) {
					if (player.getGameMode() != GameMode.CREATIVE) {
						if (player.isFlying()) {
							player.setFlying(false);
						}
					}
					if (player.getGameMode() != GameMode.CREATIVE) {
						if (player.getAllowFlight()) {
							player.setAllowFlight(false);
							flying.add(player);
						}
					}
					RankQuest quest = getQuestFromPlayer(player);
					if (quest.getTime() > 1) {
						quest.setTime(quest.getTime() - 1);
						ItemStack cditem = itemhandler.makeCdQuest(player, quest.getRank(), 1, quest.getTime());
						player.getInventory().setItem(slot, cditem);
						return;
					} else if (getQuestFromPlayer(player).getTime() == 1) {
						stopQuest(player);
						return;
					} else {
						getQuestFromPlayer(player).setTime(getQuestFromPlayer(player).getTime() - 1);
						return;
					}
				} else {
					return;
				}
			}
		}, 20, 20);
		rankquest.setTimer(timer);
		return;
	}

	/**
	 * Stops rank quest for specified player.
	 * 
	 * @param player
	 *            Player to stop rank quest for.
	 */
	public void stopQuest(Player player) {
		RankQuest rankquest = getQuestFromPlayer(player);
		Rank rank = rankquest.getRank();
		int slot = rankquest.getSlot();
		int amount = 0;
		if (!config.isSplit()) {
			amount = config.getConfigInt("RankQuestOptions.Ranks." + rank.getName() + ".Voucher.Amount");
			if (amount > 0) {
				ItemStack voucher = itemhandler.makeVoucher(player, rank, amount);
				player.getInventory().setItem(slot, voucher);
				messagehandler.endQuest(player, rank);
			}
		} else {
			amount = config.getQuestsInt("Quests." + rank.getName() + ".Voucher.Amount");
			if (amount > 0) {
				ItemStack voucher = itemhandler.makeVoucher(player, rank, amount);
				player.getInventory().setItem(slot, voucher);
				messagehandler.endQuest(player, rank);
			}
		}
		Bukkit.getScheduler().cancelTask(rankquest.getTimer());
		if (flying.contains(player)) {
			player.setAllowFlight(true);
			flying.remove(player);
		}
		rankquests.remove(rankquest);
		return;
	}

	/**
	 * Stops rank quest for dead player.
	 * 
	 * @param player
	 *            Player to stop rank quest for.
	 * @param drops
	 *            Drops from dead player.
	 */
	public void interruptDeathQuest(Player player, ArrayList<ItemStack> drops) {
		RankQuest rankquest = getQuestFromPlayer(player);
		ItemStack item = itemhandler.makeQuest(player, rankquest.getRank(), 1);
		ItemStack qitem = itemhandler.makeCdQuest(player, rankquest.getRank(), 1, rankquest.getTime());
		for (ItemStack drop : drops) {
			if (drop.isSimilar(qitem)) {
				ItemMeta dropMeta = drop.getItemMeta();
				dropMeta.setDisplayName(item.getItemMeta().getDisplayName());
				dropMeta.setLore(item.getItemMeta().getLore());
				drop.setItemMeta(dropMeta);
				break;
			} else {
				continue;
			}
		}
		Bukkit.getScheduler().cancelTask(rankquest.getTimer());
		if (flying.contains(player)) {
			player.setAllowFlight(true);
			flying.remove(player);
		}
		rankquests.remove(rankquest);
		return;
	}

	/**
	 * Stops rank quest for invalid player.
	 * 
	 * @param player
	 *            Player to stop rank quest for.
	 */
	public void interruptQuest(Player player) {
		RankQuest rankquest = getQuestFromPlayer(player);
		config.setLoggersString(("Loggers." + player.getName() + ".Slot"), (rankquest.getSlot() + ""));
		config.setLoggersString(("Loggers." + player.getName() + ".Rank"), rankquest.getRank().getName());
		if (config.getConfigString("RankQuestOptions.DropOnDC").equalsIgnoreCase("true")) {
			ItemStack item = itemhandler.makeQuest(player, rankquest.getRank(), 1);
			player.getWorld().dropItemNaturally(player.getLocation(), item);
		}
		Bukkit.getScheduler().cancelTask(rankquest.getTimer());
		rankquests.remove(rankquest);
		return;
	}

	/**
	 * Cancels rank quest for specified player.
	 * 
	 * @param player
	 *            Player to cancel rank quest for.
	 */
	public void cancelQuest(Player player) {
		RankQuest rankquest = getQuestFromPlayer(player);
		ItemStack item = itemhandler.makeQuest(player, rankquest.getRank(), 1);
		player.getInventory().setItem(rankquest.getSlot(), item);
		Bukkit.getScheduler().cancelTask(rankquest.getTimer());
		messagehandler.notWarzone(player);
		rankquests.remove(rankquest);
		return;
	}

	/**
	 * Returns a player who logs out midway through a quest the quest item.
	 * 
	 * @param player
	 *            Player to return the quest item to.
	 * @param slot
	 *            Slot to set quest item to.
	 * @param rank
	 *            Rank of rank quest to be made.
	 */
	public void handleInterruptedQuest(Player player, int slot, Rank rank) {
		if (config.getConfigString("RankQuestOptions.DropOnDC").equalsIgnoreCase("true")) {
			player.getInventory().setItem(slot, new ItemStack(Material.AIR));
			return;
		} else {
			ItemStack item = itemhandler.makeQuest(player, rank, 1);
			player.getInventory().setItem(slot, item);
			return;
		}
	}

	public void cancelAllQuests() {
		if (rankquests.size() > 0) {
			try {
				for (RankQuest rankquest : rankquests) {
					cancelQuest(rankquest.getPlayer());
				}
			} catch (ConcurrentModificationException e) {
				return;
			}
		}
	}

	public Boolean canQuest(Player player, Rank rank) {
		if (support.isInWarzone(player, rank)) {
			return true;
		} else {
			return false;
		}
	}
}
