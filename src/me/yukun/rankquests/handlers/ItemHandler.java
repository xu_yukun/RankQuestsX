package me.yukun.rankquests.handlers;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.yukun.rankquests.Config;
import me.yukun.rankquests.Methods;
import me.yukun.rankquests.Rank;

public class ItemHandler {
	private Methods methods = Methods.getInstance();
	private Config config = Config.getInstance();
	private static ItemHandler instance = new ItemHandler();

	public static ItemHandler getInstance() {
		return instance;
	}

	public ItemStack makeQuest(Player player, Rank rank, int amount) {
		if (!config.isSplit()) {
			String sitemtype = config.getConfigString("RankQuestOptions.QuestItemType");
			Material itemtype = Material.getMaterial(sitemtype);
			ItemStack item = new ItemStack(itemtype);
			String itemname = methods.color(methods.replacePHolders(config.getConfigString("RankQuestOptions.Name"), player, rank));
			ArrayList<String> itemlore = new ArrayList<String>();
			for (String line : config.getConfigStringList("RankQuestOptions.Lore")) {
				itemlore.add(methods.color(methods.replacePHolders(line, player, rank)));
			}
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(itemname);
			itemMeta.setLore(itemlore);
			item.setItemMeta(itemMeta);
			item.setAmount(amount);
			return item;
		} else {
			String sitemtype = config.getQuestsString("Quests." + rank.getName() + ".QuestItemType");
			Material itemtype = Material.getMaterial(sitemtype);
			ItemStack item = new ItemStack(itemtype);
			String itemname = methods.color(methods.replacePHolders(config.getQuestsString("Quests." + rank.getName() + ".Name"), player, rank));
			ArrayList<String> itemlore = new ArrayList<String>();
			for (String line : config.getQuestsStringList("Quests." + rank.getName() + ".Lore")) {
				itemlore.add(methods.color(methods.replacePHolders(line, player, rank)));
			}
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(itemname);
			itemMeta.setLore(itemlore);
			item.setItemMeta(itemMeta);
			item.setAmount(amount);
			return item;
		}
	}

	public ItemStack makeCdQuest(Player player, Rank rank, int amount, int time) {
		if (!config.isSplit()) {
			String sitemtype = config.getConfigString("RankQuestOptions.QuestItemType");
			Material itemtype = Material.getMaterial(sitemtype);
			ItemStack item = new ItemStack(itemtype);
			String itemname = methods.color(methods.replacePHolders(config.getConfigString("RankQuestOptions.CdName"), player, rank).replace("%time%", time + ""));
			ArrayList<String> itemlore = new ArrayList<String>();
			for (String line : config.getConfigStringList("RankQuestOptions.CdLore")) {
				itemlore.add(methods.color(methods.replacePHolders(line, player, rank).replace("%time%", time + "")));
			}
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(itemname);
			itemMeta.setLore(itemlore);
			item.setItemMeta(itemMeta);
			item.setAmount(amount);
			return item;
		} else {
			String sitemtype = config.getQuestsString("Quests." + rank.getName() + ".QuestItemType");
			Material itemtype = Material.getMaterial(sitemtype);
			ItemStack item = new ItemStack(itemtype);
			String itemname = methods.color(methods.replacePHolders(config.getQuestsString("Quests." + rank.getName() + ".CdName"), player, rank).replace("%time%", time + ""));
			ArrayList<String> itemlore = new ArrayList<String>();
			for (String line : config.getQuestsStringList("Quests." + rank.getName() + ".CdLore")) {
				itemlore.add(methods.color(methods.replacePHolders(line, player, rank).replace("%time%", time + "")));
			}
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(itemname);
			itemMeta.setLore(itemlore);
			item.setItemMeta(itemMeta);
			item.setAmount(amount);
			return item;
		}
	}

	public ItemStack makeVoucher(Player player, Rank rank, int amount) {
		if (!config.isSplit()) {
			String sitemtype = config.getConfigString("RankQuestOptions.Ranks." + rank.getName() + ".Voucher.VoucherItemType");
			Material itemtype = Material.getMaterial(sitemtype);
			ItemStack item = new ItemStack(itemtype);
			String itemname = methods.color(methods.replacePHolders(config.getConfigString("RankQuestOptions.Ranks." + rank.getName() + ".Voucher.Name"), player, rank));
			ArrayList<String> itemlore = new ArrayList<String>();
			for (String line : config.getConfigStringList("RankQuestOptions.Ranks." + rank.getName() + ".Voucher.Lore")) {
				itemlore.add(methods.color(methods.replacePHolders(line, player, rank)));
			}
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(itemname);
			itemMeta.setLore(itemlore);
			item.setItemMeta(itemMeta);
			item.setAmount(amount);
			return item;
		} else {
			String sitemtype = config.getQuestsString("Quests." + rank.getName() + ".Voucher.VoucherItemType");
			Material itemtype = Material.getMaterial(sitemtype);
			ItemStack item = new ItemStack(itemtype);
			String itemname = methods.color(methods.replacePHolders(config.getQuestsString("Quests." + rank.getName() + ".Voucher.Name"), player, rank));
			ArrayList<String> itemlore = new ArrayList<String>();
			for (String line : config.getQuestsStringList("Quests." + rank.getName() + ".Voucher.Lore")) {
				itemlore.add(methods.color(methods.replacePHolders(line, player, rank)));
			}
			ItemMeta itemMeta = item.getItemMeta();
			itemMeta.setDisplayName(itemname);
			itemMeta.setLore(itemlore);
			item.setItemMeta(itemMeta);
			item.setAmount(amount);
			return item;
		}
	}
}