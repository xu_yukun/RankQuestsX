package me.yukun.rankquests.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import me.yukun.rankquests.Rank;
import me.yukun.rankquests.RankQuest;
import me.yukun.rankquests.handlers.ItemHandler;
import me.yukun.rankquests.handlers.MessageHandler;
import me.yukun.rankquests.handlers.QuestHandler;
import me.yukun.rankquests.multisupport.Support;

public class QuestMoveEvents implements Listener {
	private Support support = Support.getInstance();
	private ItemHandler itemhandler = ItemHandler.getInstance();
	private QuestHandler questhandler = QuestHandler.getInstance();
	private MessageHandler messagehandler = MessageHandler.getInstance();

	@EventHandler
	public void playerLeaveWarzoneEvent(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		if (questhandler.getQuestFromPlayer(player) != null) {
			Rank rank = questhandler.getQuestFromPlayer(player).getRank();
			if (!support.isInWarzone(player, rank)) {
				questhandler.cancelQuest(player);
				return;
			}
		}
	}

	@EventHandler
	public void questClickEvent(InventoryClickEvent e) {
		if (e.getWhoClicked() instanceof Player) {
			Player player = (Player) e.getWhoClicked();
			if (questhandler.getQuestFromPlayer(player) != null) {
				int slot = e.getSlot();
				int qslot = questhandler.getQuestFromPlayer(player).getSlot();
				if (qslot == slot) {
					e.setCancelled(true);
					messagehandler.noMoveQuestItem(player);
					return;
				} else if (e.getClick() == ClickType.NUMBER_KEY) {
					if (e.getHotbarButton() == qslot) {
						e.setCancelled(true);
						messagehandler.noMoveQuestItem(player);
						return;
					}
					return;
				}
				return;
			}
			return;
		}
		return;
	}

	@EventHandler
	public void questDropEvent(PlayerDropItemEvent e) {
		if (e.getPlayer() instanceof Player) {
			Player player = e.getPlayer();
			if (questhandler.getQuestFromPlayer(player) != null) {
				RankQuest rankquest = questhandler.getQuestFromPlayer(player);
				ItemStack item = e.getItemDrop().getItemStack();
				ItemStack questitem = itemhandler.makeCdQuest(player, rankquest.getRank(), 1, rankquest.getTime());
				if (item.isSimilar(questitem)) {
					e.setCancelled(true);
					messagehandler.noDropQuestItem(player);
					return;
				}
				return;
			}
			return;
		}
		return;
	}
}