package me.yukun.rankquests.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

import me.yukun.rankquests.handlers.MessageHandler;
import me.yukun.rankquests.handlers.QuestHandler;

public class DualWieldQuestMoveEvent implements Listener {
	private QuestHandler questhandler = QuestHandler.getInstance();
	private MessageHandler messagehandler = MessageHandler.getInstance();

	@EventHandler
	public void playerChangeHandEvent(PlayerSwapHandItemsEvent e) {
		Player player = e.getPlayer();
		if (questhandler.getQuestFromPlayer(player) != null) {
			int slot = player.getInventory().getHeldItemSlot();
			int qslot = questhandler.getQuestFromPlayer(player).getSlot();
			if (slot == qslot || qslot == 40) {
				e.setCancelled(true);
				messagehandler.noMoveQuestItem(player);
				return;
			}
		}
	}
}