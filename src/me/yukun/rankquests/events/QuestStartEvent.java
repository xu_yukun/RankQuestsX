package me.yukun.rankquests.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.yukun.rankquests.Methods;
import me.yukun.rankquests.Rank;
import me.yukun.rankquests.checks.QuestItemChecks;
import me.yukun.rankquests.handlers.MessageHandler;
import me.yukun.rankquests.handlers.QuestHandler;
import me.yukun.rankquests.multisupport.Support;

public class QuestStartEvent implements Listener {
	private Methods methods = Methods.getInstance();
	private Support support = Support.getInstance();
	private QuestHandler questhandler = QuestHandler.getInstance();
	private MessageHandler messagehandler = MessageHandler.getInstance();
	private QuestItemChecks itemchecks = QuestItemChecks.getInstance();

	@EventHandler
	public void startQuestEvent(PlayerInteractEvent e) {
		if (e.getPlayer() != null) {
			Player player = e.getPlayer();
			if (methods.getVersion() >= 191) {
				// 1.9 and above checking of main hand item.
				if (methods.getItemInHand(player) != null) {
					ItemStack mhand = methods.getItemInHand(player);
					if (itemchecks.isQuest(mhand, player) != null) {
						Rank rank = itemchecks.isQuest(mhand, player);
						if (questhandler.getQuestFromPlayer(player) != null) {
							// Prevents player from starting more than 1 rank quest.
							messagehandler.only1Quest(player);
							return;
						} else {
							if (mhand.getAmount() == 1) {
								if (support.isInWarzone(player, rank)) {
									// Player passes all checks, ready to start quest.
									// Sends quest handling off to QuestHandler.
									questhandler.startQuest(player, rank, player.getInventory().getHeldItemSlot());
									// Sends quest start message to all players.
									messagehandler.startQuest(rank, player);
									return;
								} else {
									messagehandler.notWarzone(player);
									return;
								}
							} else {
								// Prevents player from starting quest if there are multiple quest items in a slot.
								messagehandler.only1QuestItem(player);
								return;
							}
						}
					} else if (player.getInventory().getItemInOffHand() != null) {
						ItemStack ohand = player.getInventory().getItemInOffHand();
						if (itemchecks.isQuest(ohand, player) != null) {
							Rank rank = itemchecks.isQuest(ohand, player);
							if (questhandler.getQuestFromPlayer(player) != null) {
								// Prevents player from starting more than 1 rank quest.
								messagehandler.only1Quest(player);
								return;
							} else {
								if (ohand.getAmount() == 1) {
									if (support.isInWarzone(player, rank)) {
										// Player passes all checks, ready to start quest.
										// Sends quest handling off to QuestHandler.
										questhandler.startQuest(player, rank, 40);
										// Sends quest start message to all players.
										messagehandler.startQuest(rank, player);
										return;
									} else {
										messagehandler.notWarzone(player);
										return;
									}
								} else {
									// Prevents player from starting quest if there are multiple quest items in a slot.
									messagehandler.only1QuestItem(player);
									return;
								}
							}
						}
						return;
					}
					return;
					// 1.9 and above checking of off hand item.
				} else if (player.getInventory().getItemInOffHand() != null) {
					ItemStack ohand = player.getInventory().getItemInOffHand();
					if (itemchecks.isQuest(ohand, player) != null) {
						Rank rank = itemchecks.isQuest(ohand, player);
						if (questhandler.getQuestFromPlayer(player) != null) {
							// Prevents player from starting more than 1 rank quest.
							messagehandler.only1Quest(player);
							return;
						} else {
							if (ohand.getAmount() == 1) {
								if (support.isInWarzone(player, rank)) {
									// Player passes all checks, ready to start quest.
									// Sends quest handling off to QuestHandler.
									questhandler.startQuest(player, rank, 40);
									// Sends quest start message to all players.
									messagehandler.startQuest(rank, player);
									return;
								} else {
									messagehandler.notWarzone(player);
									return;
								}
							} else {
								// Prevents player from starting quest if there are multiple quest items in a slot.
								messagehandler.only1QuestItem(player);
								return;
							}
						}
					}
					return;
				}
				return;
			} else if (methods.getVersion() < 191 && methods.getItemInHand(player) != null) {
				ItemStack hand = methods.getItemInHand(player);
				// 1.8 and below checking of held item.
				if (itemchecks.isQuest(hand, player) != null) {
					Rank rank = itemchecks.isQuest(hand, player);
					if (questhandler.getQuestFromPlayer(player) != null) {
						// Prevents player from starting more than 1 rank quest.
						messagehandler.only1Quest(player);
						return;
					} else {
						if (hand.getAmount() == 1) {
							if (support.isInWarzone(player, rank)) {
								// Sends quest handling off to QuestHandler.
								questhandler.startQuest(player, rank, 40);
								// Sends quest start message to all players.
								messagehandler.startQuest(rank, player);
								return;
							} else {
								messagehandler.notWarzone(player);
								return;
							}
						} else {
							// Prevents player from starting quest if there are multiple quest items in a slot.
							messagehandler.only1QuestItem(player);
							return;
						}
					}
				}
				return;
			}
			return;
		}
		return;
	}
}