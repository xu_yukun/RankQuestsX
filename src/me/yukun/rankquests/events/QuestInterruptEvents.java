package me.yukun.rankquests.events;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import me.yukun.rankquests.Config;
import me.yukun.rankquests.checks.LoggerChecks;
import me.yukun.rankquests.handlers.QuestHandler;

public class QuestInterruptEvents implements Listener {
	private Config config = Config.getInstance();
	private QuestHandler questhandler = QuestHandler.getInstance();

	@EventHandler
	public void playerDeathEvent(PlayerDeathEvent e) {
		Player player = e.getEntity();
		if (questhandler.getQuestFromPlayer(player) != null) {
			ArrayList<ItemStack> drops = (ArrayList<ItemStack>) e.getDrops();
			questhandler.interruptDeathQuest(player, drops);
			return;
		}
		return;
	}

	@EventHandler
	public void playerQuitEvent(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		if (questhandler.getQuestFromPlayer(player) != null) {
			questhandler.interruptQuest(player);
			return;
		}
		return;
	}

	@EventHandler
	public void playerJoinEvent(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		if (LoggerChecks.isLogger(player)) {
			if (config.getLoggersInt("Loggers." + player.getName() + ".Slot") != null) {
				int slot = config.getLoggersInt("Loggers." + player.getName() + ".Slot");
				String rank = config.getLoggersString("Loggers." + player.getName() + ".Rank");
				questhandler.handleInterruptedQuest(player, slot, questhandler.getRankFromName(rank));
			}
		}
	}
}