package me.yukun.rankquests.multisupport;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.massivecraft.factions.Rel;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.MPlayer;
import com.massivecraft.massivecore.ps.PS;

import me.yukun.rankquests.Methods;
import net.redstoneore.legacyfactions.entity.FactionColl;

public class FactionsSupport {
	private Methods methods = Methods.getInstance();
	private static FactionsSupport instance = new FactionsSupport();

	public static FactionsSupport getInstance() {
		return instance;
	}

	public boolean isFriendly(Player player, Player other) {
		Faction p = MPlayer.get(player).getFaction();
		Faction o = MPlayer.get(other).getFaction();
		Rel r = MPlayer.get(player).getRelationTo(MPlayer.get(other));
		if (methods.removeColor(o.getName()).equalsIgnoreCase("Wilderness")) {
			return false;
		}
		if (!r.isFriend()) {
			return false;
		}
		if (r.isFriend()) {
			return true;
		}
		if (p == o) {
			return true;
		}
		return false;
	}

	public boolean isFriendly(Faction player, Faction other) {
		Rel r = player.getRelationTo(other);
		if (methods.removeColor(other.getName()).equalsIgnoreCase("Wilderness")) {
			return false;
		}
		if (!r.isFriend()) {
			return false;
		}
		if (r.isFriend()) {
			return true;
		}
		if (player == other) {
			return true;
		}
		return false;
	}

	public boolean inTerritory(Player P) {
		MPlayer player = MPlayer.get(P);
		if (methods.removeColor(player.getFaction().getName()).equalsIgnoreCase("Wilderness")) {
			return false;
		}
		if (player.isInOwnTerritory()) {
			return true;
		}
		return false;
	}

	public boolean isInWarzone(Player player) {
		Location loc = player.getLocation();
		Faction B = BoardColl.get().getFactionAt(PS.valueOf(loc));
		if (B == FactionColl.get().getWarZone()) {
			return true;
		}
		return false;
	}
}