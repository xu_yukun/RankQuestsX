package me.yukun.rankquests.multisupport;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import me.yukun.rankquests.Rank;

public class Support {
	private static Support instance = new Support();
	private WorldGuard_v7 worldGuard_v7;
	private LegacyFactionsSupport legacyfactions;
	private FactionsUUIDSupport factionsuuid;
	private FactionsXSupport factionsx;
	private FactionsSupport faction;

	public static Support getInstance() {
		return instance;
	}

	public void onEnable() {
		if (hasWorldGuard()) {
			worldGuard_v7 = WorldGuard_v7.getInstance();
		}
		if (hasFactions()) {
			Plugin factions = Bukkit.getServer().getPluginManager().getPlugin("Factions");
			if (factions.getDescription().getAuthors().contains("drtshock")) {
				factionsuuid = FactionsUUIDSupport.getInstance();
			} else if (factions.getDescription().getWebsite() != null) {
				if (factions.getDescription().getWebsite().equalsIgnoreCase("https://www.massivecraft.com/factions")) {
					faction = FactionsSupport.getInstance();
				}
			}
		}
		if (hasLegacyFactions()) {
			legacyfactions = LegacyFactionsSupport.getInstance();
		}
		if (hasFactionsX()) {
			factionsx = FactionsXSupport.getInstance();
		}
		return;
	}

	public Boolean hasLegacyFactions() {
		return Bukkit.getServer().getPluginManager().getPlugin("LegacyFactions") != null;
	}

	public Boolean hasFactions() {
		return Bukkit.getServer().getPluginManager().getPlugin("Factions") != null;
	}

	public Boolean hasFactionsX() {
		return Bukkit.getServer().getPluginManager().getPlugin("FactionsX") != null;
	}

	public Boolean hasWorldGuard() {
		return Bukkit.getServer().getPluginManager().getPlugin("WorldGuard") != null;
	}

	public Boolean isInWarzone(Player player, Rank rank) {
		Boolean f = rank.getCheckWarzone();
		Boolean wg = rank.getCheckWorldGuard();
		Boolean bl = rank.getCheckBlacklist();
		Boolean pvp = rank.getCheckPvP();
		Plugin factions = null;
		if (hasFactions()) {
			factions = Bukkit.getServer().getPluginManager().getPlugin("Factions");
		}
		if (hasLegacyFactions()) {
			factions = Bukkit.getServer().getPluginManager().getPlugin("LegacyFactions");
		}
		if (wg && f) {
			if ((hasFactions() || hasLegacyFactions() || hasFactionsX()) && hasWorldGuard()) {
				if (hasFactions()) {
					if (factions.getDescription().getAuthors().contains("drtshock")) {
						if (bl) {
							if ((worldGuard_v7.isInRegion(player, rank) || factionsuuid.isInWarzone(player)) && worldGuard_v7.notInRegion(player, rank)) {
								if (pvp) {
									if (worldGuard_v7.allowsPVP(player)) {
										return true;
									} else {
										return false;
									}
								}
								return true;
							}
						}
						if (bl) {
							if (worldGuard_v7.isInRegion(player, rank) && factionsuuid.isInWarzone(player)) {
								if (pvp) {
									if (worldGuard_v7.allowsPVP(player)) {
										return true;
									} else {
										return false;
									}
								}
								return true;
							}
						}
					}
					if (factions.getDescription().getWebsite() != null) {
						if (factions.getDescription().getWebsite().equalsIgnoreCase("https://www.massivecraft.com/factions")) {
							if (bl) {
								if ((worldGuard_v7.isInRegion(player, rank) || faction.isInWarzone(player)) && worldGuard_v7.notInRegion(player, rank)) {
									if (pvp) {
										if (worldGuard_v7.allowsPVP(player)) {
											return true;
										} else {
											return false;
										}
									}
									return true;
								}
							}
							if (bl) {
								if (worldGuard_v7.isInRegion(player, rank) || faction.isInWarzone(player)) {
									if (pvp) {
										if (worldGuard_v7.allowsPVP(player)) {
											return true;
										} else {
											return false;
										}
									}
									return true;
								}
							}
						}
					}
				} else if (hasLegacyFactions()) {
					if (bl) {
						if ((worldGuard_v7.isInRegion(player, rank) || legacyfactions.isInWarzone(player)) && worldGuard_v7.notInRegion(player, rank)) {
							if (pvp) {
								if (worldGuard_v7.allowsPVP(player)) {
									return true;
								} else {
									return false;
								}
							}
							return true;
						}
					}
					if (bl) {
						if (worldGuard_v7.isInRegion(player, rank) || legacyfactions.isInWarzone(player)) {
							if (pvp) {
								if (worldGuard_v7.allowsPVP(player)) {
									return true;
								} else {
									return false;
								}
							}
							return true;
						}
					}
				} else if (hasFactionsX()) {
					if (bl) {
						if ((worldGuard_v7.isInRegion(player, rank) || factionsx.isInWarzone(player)) && worldGuard_v7.notInRegion(player, rank)) {
							if (pvp) {
								if (worldGuard_v7.allowsPVP(player)) {
									return true;
								} else {
									return false;
								}
							}
							return true;
						}
					}
					if (bl) {
						if (worldGuard_v7.isInRegion(player, rank) || factionsx.isInWarzone(player)) {
							if (pvp) {
								if (worldGuard_v7.allowsPVP(player)) {
									return true;
								} else {
									return false;
								}
							}
							return true;
						}
					}
				}
			}
		}
		if (wg && !f) {
			if (bl) {
				if (worldGuard_v7.isInRegion(player, rank) && worldGuard_v7.notInRegion(player, rank)) {
					if (pvp) {
						if (worldGuard_v7.allowsPVP(player)) {
							return true;
						} else {
							return false;
						}
					}
					return true;
				}
			}
			if (bl) {
				if (worldGuard_v7.isInRegion(player, rank)) {
					if (pvp) {
						if (worldGuard_v7.allowsPVP(player)) {
							return true;
						} else {
							return false;
						}
					}
					return true;
				}
			}
		}
		if (!wg && f) {
			if (hasFactions() || hasLegacyFactions() || hasFactionsX()) {
				if (hasFactions()) {
					if (factions.getDescription().getAuthors().contains("drtshock")) {
						if (factionsuuid.isInWarzone(player)) {
							if (pvp) {
								if (worldGuard_v7.allowsPVP(player)) {
									return true;
								}
							}
							return true;
						}
						if (!factionsuuid.inTerritory(player)) {
							return false;
						}
					}
					if (factions.getDescription().getWebsite() != null) {
						if (factions.getDescription().getWebsite().equalsIgnoreCase("https://www.massivecraft.com/factions")) {
							if (faction.isInWarzone(player)) {
								if (pvp) {
									if (worldGuard_v7.allowsPVP(player)) {
										return true;
									}
								}
								return true;
							}
							if (!faction.inTerritory(player)) {
								return false;
							}
						}
					}
				} else if (hasLegacyFactions()) {
					if (legacyfactions.isInWarzone(player)) {
						if (pvp) {
							if (worldGuard_v7.allowsPVP(player)) {
								return true;
							}
						}
						return true;
					}
					if (!legacyfactions.inTerritory(player)) {
						return false;
					}
				} else if (hasFactionsX()) {
					if (factionsx.isInWarzone(player)) {
						if (pvp) {
							if (worldGuard_v7.allowsPVP(player)) {
								return true;
							}
						}
						return true;
					}
					if (!factionsx.inTerritory(player)) {
						return false;
					}
				}
			}
		}
		if (!wg && !f) {
			return true;
		}
		return false;
	}

	public Boolean isFriendly(Entity P, Entity O) {
		if (P instanceof Player && O instanceof Player) {
			Plugin factions = null;
			if (hasFactions()) {
				factions = Bukkit.getServer().getPluginManager().getPlugin("Factions");
			}
			if (hasLegacyFactions()) {
				factions = Bukkit.getServer().getPluginManager().getPlugin("LegacyFactions");
			}
			if (hasFactions() || hasLegacyFactions() || hasFactionsX()) {
				Player player = (Player) P;
				Player other = (Player) O;
				if (hasFactions()) {
					if (factions.getDescription().getAuthors().contains("drtshock")) {
						return factionsuuid.isFriendly(player, other);
					}
					if (factions.getDescription().getWebsite() != null) {
						if (factions.getDescription().getWebsite().equalsIgnoreCase("https://www.massivecraft.com/factions")) {
							return faction.isFriendly(player, other);
						}
					}
				} else if (hasLegacyFactions()) {
					return legacyfactions.isFriendly(player, other);
				} else if (hasFactionsX()) {
					return factionsx.isFriendly(player, other);
				}
			}
		}
		return false;
	}
}