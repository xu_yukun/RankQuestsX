package me.yukun.rankquests.multisupport;

import org.bukkit.entity.Player;

import me.yukun.rankquests.Methods;
import net.redstoneore.legacyfactions.Relation;
import net.redstoneore.legacyfactions.entity.Board;
import net.redstoneore.legacyfactions.entity.FPlayerColl;
import net.redstoneore.legacyfactions.entity.Faction;
import net.redstoneore.legacyfactions.entity.FactionColl;
import net.redstoneore.legacyfactions.locality.Locality;

public class LegacyFactionsSupport {
	private Methods methods = Methods.getInstance();
	private static LegacyFactionsSupport instance = new LegacyFactionsSupport();

	public static LegacyFactionsSupport getInstance() {
		return instance;
	}

	public boolean isFriendly(Player player, Player other) {
		Faction p = FPlayerColl.get(player).getFaction();
		Faction o = FPlayerColl.get(other).getFaction();
		Relation r = FPlayerColl.get(player).getRelationTo(FPlayerColl.get(other));
		if (methods.removeColor(o.getTag()).equalsIgnoreCase("Wilderness"))
			return false;
		if (p == o)
			return true;
		if (!r.isAlly())
			return false;
		if (r.isAlly())
			return true;
		return false;
	}

	public boolean inTerritory(Player P) {
		if (methods.removeColor(FPlayerColl.get(P).getFaction().getTag()).equalsIgnoreCase("Wilderness"))
			return false;
		if (FPlayerColl.get(P).isInOwnTerritory()) {
			return true;
		}
		if (FPlayerColl.get(P).isInAllyTerritory()) {
			return true;
		}
		return false;
	}

	public boolean isInWarzone(Player player) {
		Faction B = Board.get().getFactionAt(Locality.of(FPlayerColl.get(player)));
		if (B == FactionColl.get().getWarZone()) {
			return true;
		}
		return false;
	}
}