package me.yukun.rankquests.multisupport;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import me.yukun.rankquests.Rank;

public class WorldGuard_v7 {
	private WorldGuard wg = WorldGuard.getInstance();
	private Support support = Support.getInstance();
	private static WorldGuard_v7 instance = new WorldGuard_v7();

	public static WorldGuard_v7 getInstance() {
		return instance;
	}

	public boolean allowsPVP(Entity en) {
		if (support.hasWorldGuard()) {
			Location loc = en.getLocation();
			BukkitWorld world = new BukkitWorld(loc.getWorld());
			BlockVector3 v = BlockVector3.at(loc.getX(), loc.getY(), loc.getZ());
			try {
				ApplicableRegionSet set = wg.getPlatform().getRegionContainer().get(world).getApplicableRegions(v);
				return set.queryState(null, Flags.PVP) != StateFlag.State.DENY;
			} catch (NullPointerException e) {
				return true;
			}
		}
		return true;
	}

	public boolean allowsBreak(Entity en) {
		if (support.hasWorldGuard()) {
			Location loc = en.getLocation();
			BukkitWorld world = new BukkitWorld(loc.getWorld());
			BlockVector3 v = BlockVector3.at(loc.getX(), loc.getY(), loc.getZ());
			try {
				ApplicableRegionSet set = wg.getPlatform().getRegionContainer().get(world).getApplicableRegions(v);
				return set.queryState(null, Flags.BLOCK_BREAK) != StateFlag.State.DENY;
			} catch (NullPointerException e) {
				return true;
			}
		}
		return true;
	}

	public boolean isInRegion(Player player, Rank rank) {
		if (support.hasWorldGuard()) {
			Location loc = player.getLocation();
			BukkitWorld world = new BukkitWorld(loc.getWorld());
			BlockVector3 v = BlockVector3.at(loc.getX(), loc.getY(), loc.getZ());
			for (ProtectedRegion set : wg.getPlatform().getRegionContainer().get(world).getApplicableRegions(v)) {
				if (set != null) {
					for (String id : rank.getRegions()) {
						if (id != null && set.getId().equalsIgnoreCase(id)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public boolean notInRegion(Player player, Rank rank) {
		if (support.hasWorldGuard()) {
			Location loc = player.getLocation();
			BukkitWorld world = new BukkitWorld(loc.getWorld());
			BlockVector3 v = BlockVector3.at(loc.getX(), loc.getY(), loc.getZ());
			for (ProtectedRegion set : wg.getPlatform().getRegionContainer().get(world).getApplicableRegions(v)) {
				if (set != null) {
					for (String id : rank.getBlacklist()) {
						if (id != null && set.getId().equalsIgnoreCase(id)) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}
}