package me.yukun.rankquests.multisupport;

import org.bukkit.entity.Player;

import net.prosavage.factionsx.core.FPlayer;
import net.prosavage.factionsx.core.Faction;
import net.prosavage.factionsx.manager.GridManager;
import net.prosavage.factionsx.manager.PlayerManager;
import net.prosavage.factionsx.util.Relation;

public class FactionsXSupport {
	private GridManager gridManager = GridManager.INSTANCE;
	private PlayerManager playerManager = PlayerManager.INSTANCE;
	private static FactionsXSupport instance = new FactionsXSupport();

	public static FactionsXSupport getInstance() {
		return instance;
	}

    public Boolean isFriendly(Player player, Player other) {
        FPlayer fPlayer = playerManager.getFPlayer(player);
        FPlayer fOther = playerManager.getFPlayer(other);
        if (fPlayer == null || fOther == null || fOther.getFaction().isWilderness()) {
            return false;
        }
        Faction factionPlayer = fPlayer.getFaction();
        Faction factionOther = fOther.getFaction();
        Relation relation = factionPlayer.getRelationTo(factionOther);
        return factionPlayer.getId() == factionOther.getId() || relation == Relation.ALLY || relation == Relation.TRUCE;
    }

    public boolean inTerritory(Player player) {
        Faction factionPlayer = playerManager.getFPlayer(player).getFaction();
        Faction factionBlock = gridManager.getFactionAt(player.getLocation().getChunk());
        if (!factionBlock.isWilderness() && factionPlayer.getId() == factionBlock.getId()) {
        	return true;
        }
        if (factionPlayer.getRelationTo(factionBlock) == Relation.ALLY) {
        	return true;
        }
        return false;
    }

	public Boolean isInWarzone(Player player) {
		return gridManager.getFactionAt(player.getLocation().getChunk()).isWarzone();
	}
}