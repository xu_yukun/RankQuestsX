package me.yukun.rankquests.multisupport;

import org.bukkit.entity.Player;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.perms.Relation;

import me.yukun.rankquests.Methods;

public class FactionsUUIDSupport {
	private Methods methods = Methods.getInstance();
	private static FactionsUUIDSupport instance = new FactionsUUIDSupport();

	public static FactionsUUIDSupport getInstance() {
		return instance;
	}

	public boolean isFriendly(Player player, Player other) {
		Faction p = FPlayers.getInstance().getByPlayer(player).getFaction();
		Faction o = FPlayers.getInstance().getByPlayer(other).getFaction();
		Relation r = FPlayers.getInstance().getByPlayer(player).getRelationTo(FPlayers.getInstance().getByPlayer(other));
		if (methods.removeColor(o.getTag()).equalsIgnoreCase("Wilderness")) {
			return false;
		}
		if (p == o) {
			return true;
		}
		if (!r.isAlly()) {
			return false;
		}
		if (r.isAlly()) {
			return true;
		}
		return false;
	}

	public boolean inTerritory(Player P) {
		if (methods.removeColor(FPlayers.getInstance().getByPlayer(P).getFaction().getTag()).equalsIgnoreCase("Wilderness")) {
			return false;
		}
		if (FPlayers.getInstance().getByPlayer(P).isInOwnTerritory()) {
			return true;
		}
		if (FPlayers.getInstance().getByPlayer(P).isInAllyTerritory()) {
			return true;
		}
		return false;
	}

	public boolean isInWarzone(Player player) {
		return Board.getInstance().getFactionAt(new FLocation(player.getLocation())).isWarZone();
	}
}