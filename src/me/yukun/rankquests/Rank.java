package me.yukun.rankquests;

import java.util.ArrayList;

public class Rank {
	private String name;
	private String rankname;
	private Integer maxtime;
	private Boolean checkWarzone;
	private Boolean checkWorldGuard;
	private Boolean checkBlacklist;
	private Boolean checkPvP;
	private ArrayList<String> regions;
	private ArrayList<String> blacklist;

	public Rank(String name, String rankname, Integer maxtime, Boolean checkWarzone, Boolean checkWorldGuard, Boolean checkBlacklist, Boolean checkPvP, ArrayList<String> regions, ArrayList<String> blacklist) {
		this.name = name;
		this.rankname = rankname;
		this.maxtime = maxtime;
		this.checkWarzone = checkWarzone;
		this.checkWorldGuard = checkWorldGuard;
		this.checkBlacklist = checkBlacklist;
		this.checkPvP = checkPvP;
		this.regions = regions;
		this.blacklist = blacklist;
	}

	public Rank setName(String name) {
		this.name = name;
		return this;
	}

	public Rank setRankname(String rankname) {
		this.rankname = rankname;
		return this;
	}

	public Rank setMaxtime(Integer maxtime) {
		this.maxtime = maxtime;
		return this;
	}

	public Rank setCheckWarzone(Boolean checkWarzone) {
		this.checkWarzone = checkWarzone;
		return this;
	}

	public Rank setCheckWorldGuard(Boolean checkWorldGuard) {
		this.checkWorldGuard = checkWorldGuard;
		return this;
	}

	public Rank setCheckBlacklist(Boolean checkBlacklist) {
		this.checkBlacklist = checkBlacklist;
		return this;
	}

	public Rank setCheckPvP(Boolean checkPvP) {
		this.checkPvP = checkPvP;
		return this;
	}

	public Rank setRegions(ArrayList<String> regions) {
		this.regions = regions;
		return this;
	}

	public Rank setBlacklist(ArrayList<String> blacklist) {
		this.blacklist = blacklist;
		return this;
	}

	public String getName() {
		return name;
	}

	public String getRankname() {
		return rankname;
	}

	public Integer getMaxtime() {
		return maxtime;
	}

	public Boolean getCheckWarzone() {
		return checkWarzone;
	}

	public Boolean getCheckWorldGuard() {
		return checkWorldGuard;
	}

	public Boolean getCheckBlacklist() {
		return checkBlacklist;
	}

	public Boolean getCheckPvP() {
		return checkPvP;
	}

	public ArrayList<String> getRegions() {
		return regions;
	}

	public ArrayList<String> getBlacklist() {
		return blacklist;
	}
}