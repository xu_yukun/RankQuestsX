package me.yukun.rankquests;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.yukun.rankquests.handlers.ItemHandler;
import me.yukun.rankquests.handlers.MessageHandler;
import me.yukun.rankquests.handlers.QuestHandler;

public class Vouchers implements Listener {
	private Config config = Config.getInstance();
	private Methods methods = Methods.getInstance();
	private ItemHandler itemhandler = ItemHandler.getInstance();
	private QuestHandler questhandler = QuestHandler.getInstance();
	private MessageHandler messagehandler = MessageHandler.getInstance();

	@EventHandler
	public void voucherUseEvent(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (methods.getItemInHand(player) != null) {
				ItemStack item = methods.getItemInHand(player);
				for (Rank ranks : questhandler.getAllRanks()) {
					ItemStack voucher = itemhandler.makeVoucher(player, ranks, 1);
					if (voucher.isSimilar(item)) {
						e.setCancelled(true);
						messagehandler.voucherUse(player, ranks);
						if (item.getAmount() == 1) {
							methods.setItemInHand(player, null);
						}
						if (item.getAmount() > 1) {
							ItemStack item2 = item;
							item2.setAmount(item.getAmount() - 1);
							methods.setItemInHand(player, item2);
						}
						for (String cmds : config.getConfigStringList("RankQuestOptions.Ranks." + ranks.getName() + ".Voucher.Commands")) {
							String cmd = cmds.replace("%player%", player.getName());
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);
						}
					} else {
						continue;
					}
				}
			}
		}
	}
}