package me.yukun.rankquests.checks;

import org.bukkit.entity.Player;

import me.yukun.rankquests.Main;

public class LoggerChecks {
	public static Boolean isLogger(Player player) {
		String name = player.getName();
		if (Main.settings.getLoggers().getConfigurationSection("Loggers") != null) {
			if (Main.settings.getLoggers().getConfigurationSection("Loggers").getKeys(false).contains(name)) {
				return true;
			}
			return false;
		}
		return false;
	}
}
