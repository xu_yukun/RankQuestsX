package me.yukun.rankquests.checks;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.yukun.rankquests.Rank;
import me.yukun.rankquests.handlers.ItemHandler;
import me.yukun.rankquests.handlers.QuestHandler;

public class QuestItemChecks {
	private ItemHandler itemhandler = ItemHandler.getInstance();
	private QuestHandler questhandler = QuestHandler.getInstance();
	private static QuestItemChecks instance = new QuestItemChecks();

	public static QuestItemChecks getInstance() {
		return instance;
	}

	public Rank isQuest(ItemStack item, Player player) {
		for (Rank rank : questhandler.getAllRanks()) {
			ItemStack quest = itemhandler.makeQuest(player, rank, 1);
			if (item.isSimilar(quest)) {
				return rank;
			} else {
				continue;
			}
		}
		return null;

	}
}