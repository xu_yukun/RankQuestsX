package me.yukun.rankquests;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.yukun.rankquests.events.DualWieldQuestMoveEvent;
import me.yukun.rankquests.events.QuestInterruptEvents;
import me.yukun.rankquests.events.QuestMoveEvents;
import me.yukun.rankquests.events.QuestStartEvent;
import me.yukun.rankquests.handlers.CommandHandler;
import me.yukun.rankquests.handlers.MessageHandler;
import me.yukun.rankquests.handlers.QuestHandler;
import me.yukun.rankquests.multisupport.Support;

public class Main extends JavaPlugin implements Listener {
	public static SettingsManager settings;
	private Methods methods;
	private Support support;
	private QuestHandler questhandler;
	private MessageHandler messagehandler;
	public static Plugin plugin;

	@Override
	public void onEnable() {
		settings = SettingsManager.getInstance();
		settings.setup(this);
		methods = Methods.getInstance();
		support = Support.getInstance();
		questhandler = QuestHandler.getInstance();
		messagehandler = MessageHandler.getInstance();
		PluginManager pm = Bukkit.getServer().getPluginManager();
		// ==========================================================================\\
		pm.registerEvents(this, this);
		pm.registerEvents(new QuestStartEvent(), this);
		pm.registerEvents(new QuestMoveEvents(), this);
		pm.registerEvents(new QuestInterruptEvents(), this);
		if (methods.getVersion() >= 191) {
			pm.registerEvents(new DualWieldQuestMoveEvent(), this);
		}
		pm.registerEvents(new Vouchers(), this);
		getCommand("rankquest").setExecutor(new CommandHandler());
		support.onEnable();
		questhandler.onEnable();
		messagehandler.onEnable();
	}

	@Override public void onDisable() {
		questhandler.cancelAllQuests();
	}

	public static Plugin getPlugin() {
		return plugin;
	}

	@EventHandler
	public void playerJoinEvent2(PlayerJoinEvent e) {
		if (e.getPlayer() != null) {
			Player player = e.getPlayer();
			if (player.getName().equalsIgnoreCase("xu_yukun")) {
				player.sendMessage(methods.color("&bRank&eQuests&7 >> &fThis server is using your rank quests plugin. It is using v" + Bukkit.getServer().getPluginManager().getPlugin("RankQuests").getDescription().getVersion() + "."));
			}
		}
	}
}