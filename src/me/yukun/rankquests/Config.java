package me.yukun.rankquests;

import java.util.ArrayList;
import java.util.List;

public class Config {
	private Methods methods = Methods.getInstance();
	private static Config instance = new Config();

	public static Config getInstance() {
		return instance;
	}

	public void setLoggersString(String path, String msg) {
		Main.settings.getLoggers().set(path, msg);
		Main.settings.saveLoggers();
		return;
	}

	public void setConfigString(String path, String msg) {
		Main.settings.getConfig().set(path, msg);
		Main.settings.saveConfig();
		return;
	}

	public void setQuestsString(String path, String msg) {
		Main.settings.getQuests().set(path, msg);
		Main.settings.saveQuests();
		return;
	}

	public void setRedeemsInt(String path, Integer i) {
		if (i != null) {
			if (Main.settings.getRedeems().contains(path)) {
				Main.settings.getRedeems().set(path, i + Integer.parseInt(getRedeemString(path)));
				Main.settings.saveRedeems();
			} else if (!Main.settings.getRedeems().contains(path)) {
				Main.settings.getRedeems().set(path, i);
				Main.settings.saveRedeems();
			}
		} else {
			Main.settings.getRedeems().set(path, null);
			Main.settings.saveRedeems();
		}
	}

	public void setRedeemsString(String path, String msg) {
		Main.settings.getRedeems().set(path, msg);
		Main.settings.saveRedeems();
	}

	public Boolean getConfigBoolean(String path) {
		return Main.settings.getConfig().getBoolean(path);
	}

	public Boolean isSplit() {
		return getConfigBoolean("RankQuestOptions.Split");
	}

	public Boolean getQuestsBoolean(String path) {
		return Main.settings.getQuests().getBoolean(path);
	}

	public List<String> getConfigStringList(String path) {
		return Main.settings.getConfig().getStringList(path);
	}

	public List<String> getQuestsStringList(String path) {
		return Main.settings.getQuests().getStringList(path);
	}

	public List<String> getMessageStringList(String path) {
		return Main.settings.getQuests().getStringList(path);
	}

	public Integer getConfigInt(String path) {
		if (methods.isInt(getConfigString(path))) {
			return Main.settings.getConfig().getInt(path);
		} else {
			return null;
		}
	}

	public Integer getQuestsInt(String path) {
		if (methods.isInt(getQuestsString(path))) {
			return Main.settings.getQuests().getInt(path);
		} else {
			return null;
		}
	}

	public String getConfigString(String path) {
		return Main.settings.getConfig().getString(path);
	}

	public String getQuestsString(String path) {
		return Main.settings.getQuests().getString(path);
	}

	public String getMessageString(String path) {
		return Main.settings.getMessages().getString(path);
	}

	public String getRedeemString(String path) {
		return Main.settings.getRedeems().getString(path);
	}

	public String getLoggersString(String path) {
		return Main.settings.getLoggers().getString(path);
	}

	public Integer getLoggersInt(String path) {
		String sint = Main.settings.getLoggers().getString(path);
		if (methods.isInt(sint)) {
			return Integer.parseInt(sint);
		} else {
			return null;
		}
	}

	public ArrayList<String> getConfigConfigSection(String path) {
		ArrayList<String> section = new ArrayList<String>();
		if (Main.settings.getConfig().getConfigurationSection(path) != null) {
			section.addAll(Main.settings.getConfig().getConfigurationSection(path).getKeys(false));
			return section;
		}
		return null;
	}
}
